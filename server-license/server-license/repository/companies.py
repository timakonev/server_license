from db.companies import companies
from repository.base import BaseRepository
from typing import List, Optional
from db.models.companies import Company


class CompaniesRepository(BaseRepository):

    async def get_all(self) -> List[Company]:
        query = companies.select()
        return await self.database.fetch_all(query=query)

    async def get_by_uuid_companyName(self, uuid: str, companyName: str) -> Optional[Company]:
        query = companies.select().where(companies.c.uuid == uuid).where(companies.c.companyName == companyName)
        company = await self.database.fetch_one(query=query)
        return company

    async def create(self):
        return

    async def update(self):
        return


