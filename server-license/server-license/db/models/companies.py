import datetime
from typing import Optional
from pydantic import BaseModel

class Company(BaseModel):
    id: Optional[str] = None
    uuid: str
    companyName: str
    licenseId: str
    licenseExpirationDate: datetime.datetime
