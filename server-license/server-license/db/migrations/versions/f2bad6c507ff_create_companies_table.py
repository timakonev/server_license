"""create companies table

Revision ID: f2bad6c507ff
Revises: 
Create Date: 2022-04-27 11:59:34.634375

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f2bad6c507ff'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "companies",
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True, unique=True),
        sa.Column("uuid", sa.String, unique=True),
        sa.Column("companyName", sa.String, unique=True),
        sa.Column("licenseId", sa.String, primary_key=True, unique=True),
        sa.Column("licenseExpirationDate", sa.Date),
    )


def downgrade():
    op.drop_table("companies")
