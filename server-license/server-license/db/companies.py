import sqlalchemy
from . import metadata

companies = sqlalchemy.Table(
    "companies",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True, autoincrement=True, unique=True),
    sqlalchemy.Column("uuid", sqlalchemy.String, unique=True),
    sqlalchemy.Column("companyName", sqlalchemy.String, unique=True),
    sqlalchemy.Column("licenseId", sqlalchemy.String, primary_key=True, unique=True),
    sqlalchemy.Column("licenseExpirationDate", sqlalchemy.Date),
)