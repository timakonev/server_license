from databases import Database
from sqlalchemy import MetaData
from sqlalchemy.ext.asyncio import create_async_engine

from core.config import DATABASE_URL

database = Database(DATABASE_URL)
metadata = MetaData()
engine = create_async_engine(
    DATABASE_URL,
    pool_size = 10,
)
