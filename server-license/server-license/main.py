import uvicorn
from fastapi import FastAPI
from controller import companies
import db


app = FastAPI()
app.include_router(companies.router)


@app.on_event("startup")
async def startup():
    await db.database.connect()


@app.on_event("shutdown")
async def shutdown():
    await db.database.disconnect()


if __name__ == "__main__":
    uvicorn.run("main:app", port=8280, host="0.0.0.0", reload=True)








