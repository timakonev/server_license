import datetime
from fastapi import status, HTTPException
from repository.companies import CompaniesRepository
from db.base import database


async def companies_validator(name, key):
    data = await CompaniesRepository(database).get_by_uuid_companyName(key, name)
    if data is not None:
        if data.licenseExpirationDate > datetime.date.today():
            raise HTTPException(status_code=status.HTTP_200_OK, detail="license is valid")
        else:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="license expired")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Company not found")
