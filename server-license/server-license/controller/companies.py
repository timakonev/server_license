from fastapi import APIRouter, Query
from service.companies_validator import companies_validator

router = APIRouter()
router = APIRouter(
    prefix="/check_company",
    tags=["check_company", "companies"],
)

# @router.get("/")
# async def check_company_license(name: str = Query(None), key: str = Query(None)):
#     return companies_validator(name, key)
@router.get("/")
async def check_company_license(name: str = Query(None), key: str = Query(None)):
    return await companies_validator(name, key)